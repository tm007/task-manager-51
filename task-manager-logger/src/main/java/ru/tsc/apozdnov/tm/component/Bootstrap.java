package ru.tsc.apozdnov.tm.component;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.service.IReceiverService;
import ru.tsc.apozdnov.tm.listener.LogListener;
import ru.tsc.apozdnov.tm.service.ReceiverService;

public class Bootstrap {

    public void run() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        factory.setTrustAllPackages(true);
        @NotNull final IReceiverService receiverService = new ReceiverService(factory);
        receiverService.receiveLog(new LogListener());
    }

}